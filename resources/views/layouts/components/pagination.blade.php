<?php
/**
 * Created by PhpStorm.
 * User: andromeda
 * Date: 29/01/18
 * Time: 16:22
 */
?>
@if($paginator->hasPages())
<nav>
    {{--Previous Elements--}}
    <ul class="pagination justify-content-center">
        @if($paginator->onFirstPage())
            <li class="page-item disabled">
                <a class="page-link" href="#">
                    <span class="ti-arrow-left"></span>
                </a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}">
                    <span class="ti-arrow-left"></span>
                </a>
            </li>
        @endif
        {{--End Previous Elements--}}

        {{--Array of links    --}}
        @foreach($elements as $element)
            @if(is_string($element))
                <li class=""><span class="dots-1" ></span><span class="dots-2" ></span><span class="dots-3" ></span></li>
            @endif
            @if(is_array($element))
                @foreach($element as $page => $url)
                    @if($page == $paginator->currentPage())
                        <li class="page-item active">
                            <a class="page-link" href="#">{{ $page }}</a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach
        {{--  End Array of links    --}}

        {{--Next Page    --}}
        @if($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}">
                    <span class="ti-arrow-right"></span>
                </a>
            </li>
        @else
                <li class="page-item disabled">
                    <a class="page-link" href="#">
                        <span class="ti-arrow-right"></span>
                    </a>
                </li>
        @endif
    </ul>
</nav>
@endif