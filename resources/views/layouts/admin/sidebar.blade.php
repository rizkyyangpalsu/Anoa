


<!-- Sidebar -->
<aside class="sidebar sidebar-expand-lg sidebar-iconic">
    <header class="sidebar-header">
        <span class="logo text-center">
          <a href="{{ route('admin.') }}"><img src="http://thetheme.io/theadmin/assets/img/logo-icon-light.png" alt="logo"></a>
        </span>
    </header>

    <nav class="sidebar-navigation">

        <ul class="menu">

            <li class="menu-item active">
                <a class="menu-link" href="{{ route('admin.dashboard') }}">
                    <span class="icon ti-home"></span>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="menu-item">
                <a class="menu-link" href="{{ route('admin.product') }}">
                    <span class="icon ti-briefcase"></span>
                    <span class="title">Projects</span>
                </a>
            </li>

            <li class="menu-item">
                <a class="menu-link" href="{{ route('admin.user') }}">
                    <span class="icon ti-user"></span>
                    <span class="title">Clients</span>
                </a>
            </li>

            <li class="menu-item">
                <a class="menu-link" href="#">
                    <span class="icon ti-email"></span>
                    <span class="title">Messages</span>
                    <span class="badge badge-pill badge-info">2</span>
                </a>
            </li>

            <li class="menu-item">
                <a class="menu-link" href="#">
                    <span class="icon ti-settings"></span>
                    <span class="title">Settings</span>
                </a>
            </li>

        </ul>
    </nav>

</aside>
<!-- END Sidebar -->


    <!-- Topbar -->
    <header class="topbar">
      <div class="topbar-left">
        <span class="topbar-btn sidebar-toggler"><i>&#9776;</i></span>

        <a class="topbar-btn d-none d-md-block" href="#" data-provide="fullscreen tooltip" title="Fullscreen">
          <i class="material-icons fullscreen-default">fullscreen</i>
          <i class="material-icons fullscreen-active">fullscreen_exit</i>
        </a>
      </div>

      <div class="topbar-right">
        <div class="topbar-divider"></div>

        <ul class="topbar-btns">
          <li class="dropdown">
            <span class="topbar-btn" data-toggle="dropdown"><img class="avatar" src="{{ asset('img/avatar/1.jpg') }}" alt="..."></span>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="page/profile.html"><i class="ti-user"></i> Profile</a>
              <a class="dropdown-item" href="page-app/mailbox.html">
                <div class="flexbox">
                  <i class="ti-email"></i>
                  <span class="flex-grow">Inbox</span>
                  <span class="badge badge-pill badge-info">5</span>
                </div>
              </a>
              <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="page-extra/user-lock-1.html"><i class="ti-lock"></i> Lock</a>
              <a class="dropdown-item" href="page-extra/user-login-3.html"><i class="ti-power-off"></i> Logout</a>
            </div>
          </li>

          <!-- Notifications -->
          <li class="dropdown d-none d-md-block">
            <span class="topbar-btn has-new" data-toggle="dropdown"><i class="ti-bell"></i></span>
            <div class="dropdown-menu dropdown-menu-right">

              <div class="media-list media-list-hover media-list-divided media-list-xs">
                <a class="media media-new" href="#">
                  <span class="avatar bg-success"><i class="ti-user"></i></span>
                  <div class="media-body">
                    <p>New user registered</p>
                    <time datetime="2017-07-14 20:00">Just now</time>
                  </div>
                </a>

                <a class="media" href="#">
                  <span class="avatar bg-info"><i class="ti-shopping-cart"></i></span>
                  <div class="media-body">
                    <p>New order received</p>
                    <time datetime="2017-07-14 20:00">2 min ago</time>
                  </div>
                </a>

                <a class="media" href="#">
                  <span class="avatar bg-warning"><i class="ti-face-sad"></i></span>
                  <div class="media-body">
                    <p>Refund request from <b>Ashlyn Culotta</b></p>
                    <time datetime="2017-07-14 20:00">24 min ago</time>
                  </div>
                </a>

                <a class="media" href="#">
                  <span class="avatar bg-primary"><i class="ti-money"></i></span>
                  <div class="media-body">
                    <p>New payment has made through PayPal</p>
                    <time datetime="2017-07-14 20:00">53 min ago</time>
                  </div>
                </a>
              </div>

              <div class="dropdown-footer">
                <div class="left">
                  <a href="#">Read all notifications</a>
                </div>

                <div class="right">
                  <a href="#" data-provide="tooltip" title="Mark all as read"><i class="fa fa-circle-o"></i></a>
                  <a href="#" data-provide="tooltip" title="Update"><i class="fa fa-repeat"></i></a>
                  <a href="#" data-provide="tooltip" title="Settings"><i class="fa fa-gear"></i></a>
                </div>
              </div>

            </div>
          </li>
          <!-- END Notifications -->
        </ul>

      </div>
    </header>
    <!-- END Topbar -->