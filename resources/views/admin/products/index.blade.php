@php
    /**
     * Created by PhpStorm.
     * User: andromeda
     * Date: 29/01/18
     * Time: 11:16
     */
@endphp

@extends('layouts.admin.master')

@section('content')
    <main class="main-container">
        <div class="main-content">
            <header class="header">
                <div class="header-info">
                    <div class="left">
                        <h2 class="header-title">
                            <strong>Product</strong></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Product</li>
                        </ol>
                    </div>
                </div>
            </header>
            <div class="card">
                <div class="card-title">
                    <form class="lookup lookup-right lookup-lg">
                        <input type="text" placeholder="Search...">
                    </form>
                    <a href="{{ route('admin.product.create') }}" data-provide="animsition">
                        <button class="btn btn-lg btn-round btn-info">Add Product</button>
                    </a>
                </div>
                <div class="card-body">
                    <table class="table table-striped" style="text-align: center">
                        <thead>
                        <tr>
                            <th style="text-align: center">#</th>
                            <th style="text-align: center">Name</th>
                            <th style="text-align: center">Condition</th>
                            <th style="text-align: center">Gambar</th>
                            <th style="text-align: center">Created_at</th>
                            <th style="text-align: center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $key => $value)
                            <tr>
                                <td scope="row">{{ $key + 1 }}</td>
                                <td>{{ $value->name }}</td>
                                <td>
                                    @if($value->attr->condition == 'new')
                                        <span class="badge badge-success">New</span>
                                    @else
                                        <span class="badge badge-warning">Second</span>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{ route('admin.product.image', $value->slug) }}">{{ count($value->images) }}</a>
                                </td>
                                <td>{{ date('d-m-Y', strtotime($value->created_at)) }}</td>
                                <td>
                                    <a href="{{ route('admin.product.edit', $value->slug) }}">
                                        <button class="btn btn-sm btn-label btn-info">
                                            <label><i class="ti-pencil"></i></label> Edit
                                        </button>
                                    </a>
                                    <form method="post"
                                          onclick="return confirm('Apakah anda yakin ' +
                                       'ingin menghapus barang ini')"
                                          action="{{ route('admin.product.destroy', $value->slug) }}">
                                        {!! csrf_field() !!}
                                        <button class="btn btn-sm btn-label btn-danger">
                                            <label><i class="ti-trash"></i></label> Hapus
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->links('layouts.components.pagination') }}
                </div>
            </div>
        </div>
    </main>
@endsection