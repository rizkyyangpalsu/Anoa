@extends('layouts/admin/app')

@section('content')
    <body class="min-h-fullscreen bg-img center-vh p-20"
          style="background-image: url(http://thetheme.io/theadmin/assets/img/bg/2.jpg);"
          data-overlay="7">

    <div class="card card-round card-shadowed px-50 py-30 w-400px mb-0" style="max-width: 100%">
        <h5 class="text-uppercase">Sign in</h5>
        <br>

        <form class="form-type-material" method="POST" action="{{ route('admin.login') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <input type="text" class="form-control" name="email" id="username"
                       value="{{ old('email') }}">
                <label for="username">Username</label>
            </div>

            <div class="form-group">
                <input type="password" class="form-control" name="password" id="password"
                       value="{{ old('password') }}">
                <label for="password">Password</label>
            </div>

            <div class="form-group flexbox">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="remember"
                            {{ old('remember') ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Remember me</span>
                </label>

                <a class="text-muted hover-primary fs-13" href="#">Forgot password?</a>
            </div>

            <div class="form-group">
                <button class="btn btn-bold btn-block btn-primary" type="submit">Login</button>
            </div>
        </form>

        <div class="divider">Or Sign In With</div>
        <div class="text-center">
            <a class="btn btn-square btn-facebook" href="#"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-square btn-google" href="#"><i class="fa fa-google"></i></a>
            <a class="btn btn-square btn-twitter" href="#"><i class="fa fa-twitter"></i></a>
        </div>
    </div>
    </body>
@endsection