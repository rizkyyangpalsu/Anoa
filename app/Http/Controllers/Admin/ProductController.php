<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ResourceControl;
use App\Entities\Categories\Category;
use App\Entities\Image;
use App\Entities\Products\Product;
use App\Jobs\Images\CreateImage;
use App\Jobs\Images\DeleteImage;
use App\Jobs\Products\CreateProduct;
use App\Jobs\Products\DeleteProduct;
use App\Jobs\Products\UpdateProduct;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    use ResourceControl;

    public function __construct()
    {
        $this->middleware('admin')->except('logout');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $product = Product::with(['images','categories', 'attr' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }])->paginate(10);

        $this->setData('pageTitle', 'Product - Dashboard');
        $this->setData('products', $product);

        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        $data = [
            'categories' => $categories
        ];

        return view('admin.products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->dispatch(new CreateProduct($request));
        } catch (\Exception $exception) {
            return redirect()->back()->with('Error', 'Produk gagal');
        }

        return redirect()->route('admin.product')
            ->with('success', 'Produk telah dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $attr = $product->attr()->get();
        $categories = Category::all();
        $this->setData('attr', $attr);
        $this->setData('categories', $categories);
        $this->setData('product', $product);
        $this->setData('pageTitle', 'Edit barang: '. $product->name);

        return view('admin.products.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            $this->dispatch(new UpdateProduct($request, $product));
        } catch (\Exception $exception) {
            return redirect()->back()->with('errors', 'Cant update');
        }

        return redirect()->route('admin.product')
            ->with('success', 'Produk telah di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $this->dispatch(new DeleteProduct($product));
        } catch (\Exception $exception) {
            return redirect()->back()->with('errors', 'Cant Delete');
        }

        return redirect()->route('admin.product')
            ->with('success', 'Produk telah di Hapus');
    }

    public function image(Product $product)
    {
        $this->setData('product', $product);
        $this->setData('pageTitle', $product->name);

        return view('admin.products.images');
    }

    public function imageStore(Request $request, Product $product)
    {
        try {
            $this->dispatch(new CreateImage($request, $product));
        } catch (\Exception $exception) {
            return redirect()->back()->with('errors', 'Cant create');
        }

        return redirect()->route('admin.product.image', $product->slug)
            ->with('success', 'gambar telah di buat');
    }

    public function imageDestroy(Request $request,Product $product, Image $image)
    {
        try {
            $this->dispatch(new DeleteImage($request, $product, $image));
        } catch (\Exception $exception) {
            return redirect()->back()->with('errors', 'Cant Delete');
        }

        return redirect()->route('admin.product.image', $product->slug)
            ->with('success', 'gambar telah di Hapus');
    }
}
