<?php

namespace App\Jobs\Images;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Core\Patch\JobPatcher;
use App\Entities\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CreateImage extends JobPatcher
{
    protected $validateRules = [
        'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
    ];

    protected $hash;
    protected $model;

    /**
     * Create a new job instance.
     *
     * @param Request $request
     * @param ContentTableInterface $model
     */
    public function __construct(Request $request, ContentTableInterface $model)
    {
        parent::__construct($request);

        $this->hash = hash('crc32b', uniqid());
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return ContentTableInterface
     */
    public function run()
    {
        $this->model->images()->saveMany($this->uploadFiles());

        return $this->model;
    }

    protected function uploadFiles()
    {
        $uploaded = [];

        foreach ($this->request->images as $image) {
            $path = 'uploads/images';
            $realImage = hash('crc32b', uniqid()).'.'.$image->getClientOriginalExtension();

            $image->storeAs($path, $realImage);
            array_push($uploaded, new Image([
                'src' => $path . '/' . $realImage,
                'slug' => $this->getSlugByNameAndExt($image),
            ]));
        }

        return $uploaded;
    }

    protected function getSlugByNameAndExt($image)
    {
        $str = hash('crc32b',$image->getClientOriginalName(). uniqid()).'-'.Carbon::now() . '-'
            . $image->getClientOriginalExtension();
        return str_slug($str);
    }
}
