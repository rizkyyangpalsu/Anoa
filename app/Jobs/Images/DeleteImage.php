<?php

namespace App\Jobs\Images;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Core\Patch\JobPatcher;
use App\Entities\Image;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

class DeleteImage extends JobPatcher
{
    protected $model;

    protected $image;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request,ContentTableInterface $model, Image $image)
    {
        parent::__construct($request);
        $this->model = $model;
        $this->image = $image;
    }

    /**
     * Execute the job.
     *
     * @return ContentTableInterface
     * @throws \Exception
     */
    public function run()
    {
        $detach = $this->model->images()->find($this->image->id);
        $detach->delete();

        $filesystem = app()->make(Filesystem::class);
        $filesystem->delete(storage_path('app/'. $this->image->src));

        $this->image->delete();

        return $this->model;
    }
}
