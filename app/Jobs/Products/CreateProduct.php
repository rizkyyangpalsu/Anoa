<?php

namespace App\Jobs\Products;

use App\Core\Patch\JobPatcher;
use App\Entities\Products\Product;
use App\Entities\Products\ProductAttribute;
use App\Jobs\Categories\AttachCategory;
use App\Jobs\Images\CreateImage;
use Illuminate\Http\Request;


class CreateProduct extends JobPatcher
{
    protected $validateRules = [
        'name' => 'required',
        'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        'attr.*' => 'required'
    ];

    protected $product;

    protected $attr;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->product = new Product();
        $this->attr = $this->request->input('attr');
    }
    /**
     * @return mixed
     */
    public function run()
    {
        $this->product->user()->associate(auth()->user());
        /**
         * Create slug
         */
        $str = strtolower($this->request->input('name'));
        $slug = str_slug($str, '-').'-'.hash('crc32b', uniqid());

        $this->product->fill([
            'name' => $this->request->input('name'),
            'slug' => $slug
        ]);

        $this->product->save();

        $this->product->attr()->create([
            'product_id' => $this->product->id,
            'price' => $this->attr['price'],
            'weight' => $this->attr['weight'],
            'stock' => $this->attr['stock'],
            'condition' => $this->attr['condition'],
            'size' => $this->attr['size'],
            'description' => $this->attr['description']
        ]);

        return $this->product;
    }

    /**
     * When request has categories is true, will execute AttachCategory
     * and request has file is true, will execute CreateImage
     */
    public function callback()
    {
        if ($this->request->has('categories'))
            dispatch(new AttachCategory($this->request, $this->product));
        if ($this->request->hasFile('images'))
            dispatch(new CreateImage($this->request, $this->product));
    }
}
