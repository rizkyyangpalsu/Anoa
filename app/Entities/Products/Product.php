<?php

namespace App\Entities\Products;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Entities\Accounts\User;
use App\Entities\Categories\Category;
use App\Entities\Image;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements ContentTableInterface
{
    protected $table = 'products';

    protected $fillable = [
        'name','user_id' ,'category_id', 'slug'
    ];

    public function carts()
    {
        return $this->hasMany(Cart::class, 'product_id',
            'id');
    }

    public function discussions()
    {
        return $this->hasMany(Discussion::class, 'product_id',
            'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category',
            'product_id', 'category_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'product_id',
            'id');
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites',
            'product_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id',
            'id');
    }

    public function attr()
    {
        return $this->hasOne(ProductAttribute::class, 'product_id', 'id');
    }

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function scopeBySlug($query, $value)
    {
        return $query->where('slug', '=', $value)->first();
    }
}
