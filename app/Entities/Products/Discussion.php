<?php

namespace App\Entities\Products;

use App\Entities\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $table = 'discussions';

    protected $fillable = [
        'user_id', 'product_id', 'body'
    ];

    public function user()
    {
        return $this->hasMany(User::class, 'user_id',
            'id');
    }

    public function product()
    {
        return $this->hasMany(Product::class, 'product_id',
            'id');
    }
}
