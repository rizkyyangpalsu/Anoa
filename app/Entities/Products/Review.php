<?php

namespace App\Entities\Products;

use App\Entities\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable = [
        'user_id', 'product_id', 'stars',
        'title', 'body'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id',
            'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id',
            'id');
    }
}
