<?php

namespace App\Entities\Products;

use App\Entities\Categories\Category;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attributes';

    protected $fillable = [
        'product_id', 'price', 'weight', 'stock', 'condition',
        'size', 'description',
    ];

    protected $casts = [
        'price' => 'double'
    ];

    public function product()
    {
        $this->belongsTo(Product::class, 'product_id', 'id');
    }
}