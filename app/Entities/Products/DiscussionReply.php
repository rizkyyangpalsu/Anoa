<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class DiscussionReply extends Model
{
    protected $table = 'discussion_replies';

    protected $fillable = [
        'user_id', 'discussion_id', 'body'
    ];

    public function user()
    {
        return $this->hasMany(User::class, 'user_id',
            'id');
    }

    public function discussion()
    {
        return $this->hasMany(Discussion::class, 'discussion_id',
            'id');
    }
}
