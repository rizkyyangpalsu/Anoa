<?php

namespace App\Entities\Categories;

use App\Entities\Products\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'name'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category',
            'category_id', 'product_id');
    }
}
