<?php
/**
 * Created by PhpStorm.
 * User: andromeda
 * Date: 01/02/18
 * Time: 18:54
 */
namespace App\Core\Controllers;

use Illuminate\Support\Facades\View;

trait ResourceControl
{
    public function setData($key, $value)
    {
        View::share($key, $value);

        return $this;
    }
}