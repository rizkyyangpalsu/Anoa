<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Entities\Accounts\User;
use App\Entities\Image;
use App\Entities\Products\Product;

Route::get('/', function () {
   dd(App());
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index');

Route::get('files/{path}', 'MediaController@file')->name('media.proxy')
    ->where(['path' => '.*']);

//Admin routes
Route::group([
    'as' => 'admin.',
    'prefix' => 'sysadmin',
    'namespace' => 'Admin'
], function () {
    Route::get('/login.html', 'LoginController@showLoginForm')->name('login');
    Route::post('/login.html', 'LoginController@login');
    Route::post('/logout.html', 'LoginController@logout')->name('logout');
//    Dashboard
    Route::get('/dashboard.html', 'DashboardController@index')->name('dashboard');
//    End Dashboard

//    Products
    Route::get('/product.html', 'ProductController@index')->name('product');
    Route::get('/product.html/create.html', 'ProductController@create')
        ->name('product.create');
    Route::post('/product.html/store.html', 'ProductController@store')
        ->name('product.store');
    Route::get('product.html/{product}/edit.html', 'ProductController@edit')
        ->name('product.edit');
    Route::post('product.html/{product}/edit.html', 'ProductController@update')
        ->name('product.update');
    Route::post('product.html/{product}/delete.html', 'ProductController@destroy')
        ->name('product.destroy');
    Route::get('product.html/{product}/image.html', 'ProductController@image')
        ->name('product.image');
    Route::post('product.html/{product}/image.html', 'ProductController@imageStore')
        ->name('product.image.store');
    Route::post('product.html/{product}/image.html/{image}', 'ProductController@imageDestroy')
        ->name('product.image.destroy');
    Route::bind('product', function ($value) {
        $product = Product::bySlug($value);

        return ($product instanceof Product) ? $product : abort(404);
    });
    Route::bind('image', function ($value) {
        $product = Image::bySlug($value);

        return ($product instanceof Image) ? $product : abort(404);
    });
    //    end Products

    //    Start Users
    Route::get('user.html', 'UserController@index')->name('user');
});