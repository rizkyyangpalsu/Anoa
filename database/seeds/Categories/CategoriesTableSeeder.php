<?php

use App\Entities\Categories\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    protected $data = [
        [
            'name' => 'Sepatu',
        ],[
            'name' => 'Baju',
        ],[
            'name' => 'Tas',
        ],[
            'name' => 'Aksesoris',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            Category::create($datum);
        }
    }
}
