<?php

use App\Entities\Products\ProductAttribute;
use Illuminate\Database\Seeder;

class ProductAttributesTableSeeder extends Seeder
{
    protected $data = [
        [
            'product_id' => 1,
            'price' => 200000,
            'weight' => 200,
            'stock' => 10,
            'condition' => 'new',
            'size' => '28, 29, 30, 31, 32',
            'description' => 'Ini sepatu. Wes ero :v',
        ],[
            'product_id' => 2,
            'price' => 20000,
            'weight' => 50,
            'stock' => 10,
            'description' => 'Ini gantungan kunci. Wes ero :v',
        ],[
            'product_id' => 3,
            'price' => 75000,
            'weight' => 100,
            'stock' => 10,
            'size' => 'S, M, L, XL',
            'description' => 'Ini kaos. Wes ero :v',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            $attr = ProductAttribute::create($datum);
        }
    }
}
