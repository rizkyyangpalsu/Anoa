<?php

use App\Entities\Products\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    protected $data = [
        [
            'user_id' => 1,
            'name' => 'Sepatu adibas Runner',
            'slug' => 'Sepatu-adibas-runner-xj23l.html',
            'categories' => [1]
        ],[
            'user_id' => 1,
            'name' => 'Gantungan kunci adidas',
            'slug' => 'gantungan-kunci-adidas-das34.html',
            'categories' => [4]
        ],[
            'user_id' => 1,
            'name' => 'Kaos anti peluru',
            'slug' => 'kaos-anti-peluru-ksa3ws.html',
            'categories' => [2]
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            $category = $datum['categories'];
            $user = $datum['user_id'];
            array_forget($datum, ['categories']);

            $product = Product::create($datum);

            $product->categories()->sync($category);
            $product->user()->associate($user);
        }
    }
}
